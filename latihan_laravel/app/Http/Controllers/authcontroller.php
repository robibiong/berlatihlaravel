<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authcontroller extends Controller
{
    //Menghandle Register
    public function reg(){
        return view ('halaman/register');
    }

    //Menghandle Wellcome methode:GET
    public function welcome(Request $request){
        $fname = $request["fname"];
        $lname = $request["lname"];
        $negara = $request["negara"];
        //dd($fname);
        return view ('halaman/welcome',["fname"=>$fname, "lname"=>$lname, "negara"=>$negara]);
    }
}
