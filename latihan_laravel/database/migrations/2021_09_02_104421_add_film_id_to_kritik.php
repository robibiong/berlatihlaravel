<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmIdToKritik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kritik', function (Blueprint $table) {
            //tambahin kolom
            $table->unsignedBigInteger('film_id');
            //tambahin forein key
            $table->foreign('film_id')->references('id')->on('film');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kritik', function (Blueprint $table) {
            //menghapus foreignkey user id antara profile dan users
            $table->dropforeign(['film_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('film_id');

        });
    }
}
