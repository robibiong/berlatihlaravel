<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmIdToPeran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peran', function (Blueprint $table) {
            //tambahin kolom
            $table->unsignedBigInteger('film_id');
            //tambahin forein key
            $table->foreign('film_id')->references('id')->on('film');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peran', function (Blueprint $table) {
            //menghapus foreignkey film id pada tabel peran
            $table->dropforeign(['film_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('film_id');
        });
    }
}
