<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile', function (Blueprint $table) {
            
            //tambahin kolom
            $table->unsignedBigInteger('user_id');
            //tambahin forein key
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile', function (Blueprint $table) {
            //menghapus foreignkey user id antara profile dan users
            $table->dropforeign(['user_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('user_id');
        });
    }
}
